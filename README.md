Find neighbor atoms, residues, etc., according to distance constraint in PDB file.
Compile with: g++ --static -O3 -std=c++11 neighb.cpp -o neighb -lm



neighb FILE1 FILE2 DIST

FILE{1,2}=PDB formatted file
DIST=minimum distance between selection1 and selection2
