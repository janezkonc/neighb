#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iomanip>
#include <boost/regex.hpp>
#include <assert.h>
using namespace std;
using namespace boost;

//map<pair<char, double> > v = { pair<char,double>('C', 1.5) };
map<char, double> vdW = {
  {'C', 1.7},
  {'N', 1.55},
  {'O', 1.52},
  {'S', 1.80},
  {'P', 1.80}
};

class Coord {
public:
  enum MolType {ligand, receptor};
private:
  MolType type;
  double c[3];
  string line;
  void set_coord(const string &line) {
    c[0] = atof(line.substr(30, 8).c_str());
    c[1] = atof(line.substr(38, 8).c_str());
    c[2] = atof(line.substr(46, 8).c_str());
  }
  int clash;
public:
  Coord() {}
  Coord(const string &l, MolType t) : line(l), type(t), clash(0) { 
    set_coord(l); 
    line.replace(62, 4, "0.00");
  }
  //  Coord(const double &x, const double &y, const double &z, MolType t) : line(""), type(t), clash(0) { c[0] = x; c[1] = y; c[2] = z; }
  double get(const int &i) const { return c[i]; }
  string get_line() const { return line; }
  int get_clash() const { return clash; }
  void add_clash() { clash++; }
  MolType get_type() const { return type; }
  char get_atom_name() const { return line[13]; }
  double distance(const Coord &crd) const { return sqrt(pow(c[0]-crd.get(0), 2) + pow(c[1]-crd.get(1), 2) + pow(c[2]-crd.get(2), 2)); }
  friend ostream& operator<< (ostream& os, const Coord& c) {
    os << "(" <<  c.get(0) << "," << c.get(1) << "," << c.get(2) <<")";
    return os;
  }
};

oclass Grid {
  typedef vector<Coord > V0;
  typedef map<int, V0 > A0;
  typedef map<int, A0 > A1;
  typedef map<int, A1 > A2;
  A2 storage;
public:
  Grid() {}
  void insert(const Coord &c) {
    storage[floor(c.get(0))][floor(c.get(1))][floor(c.get(2))].push_back(c);
  }
  vector<Coord> get(const Coord &c, const double d) { // get all points <d A away from c and not type(c)
    int i = floor(c.get(0));
    int j = floor(c.get(1));
    int k = floor(c.get(2));
    int i_min = floor(c.get(0) - d);
    int j_min = floor(c.get(1) - d);
    int k_min = floor(c.get(2) - d);
    int i_max = floor(c.get(0) + d);
    int j_max = floor(c.get(1) + d);
    int k_max = floor(c.get(2) + d);
    V0 vc;
    for (i = i_min; i <= i_max; i++)
      for (j = j_min; j <= j_max; j++)
	for (k = k_min; k <= k_max; k++)
	  for (auto c2 : storage[i][j][k]) {
	    if (c.get_type() != c2.get_type())
	      vc.push_back(c2);
	  }
    return vc;
  }
  const void print() {
    for (auto a2 : storage) {
      for (auto a1 : storage[a2.first]) {
	for (auto a0 : storage[a2.first][a1.first]) {
	  for (auto c : storage[a2.first][a1.first][a0.first]) {
	    cout << "v[" << a2.first << "," << a1.first << "," << a0.first << "]=" << c << endl;
	  }
	}
      }
    }
  }
};

int main(int argc, char* argv[]) {
  assert(argc == 3 || argc == 4);
  regex e1(argv[2]);
  regex e2(argc == 4 ? argv[3] : "^HETATM");
  ifstream myfile;
  myfile.open (argv[1]);
  if (!myfile.is_open()) exit(1);
  string line;
  vector<Coord> receptor, ligand;
  Grid grid;
  // read pdb
  while (getline (myfile,line)) {
    if (regex_search(line, e1)) {
      if (vdW.find(Coord(line, Coord::receptor).get_atom_name()) != vdW.end()) {
	receptor.push_back(Coord(line, Coord::receptor));
	grid.insert(receptor.back());
      }
    }
    if (regex_search(line, e2)) {
      if (vdW.find(Coord(line, Coord::ligand).get_atom_name()) != vdW.end()) {
	ligand.push_back(Coord(line, Coord::ligand));
	grid.insert(ligand.back());
      }
    }
  }
  
  // calculate number of clashed atoms between receptor and ligand
  vector<int> num_clashes;
  for (Coord &cligand : ligand) {
    vector <Coord> nearby = grid.get(cligand, 4.0); 
    for (Coord &creceptor : nearby) {
      if (vdW[creceptor.get_atom_name()] + vdW[cligand.get_atom_name()] - cligand.distance(creceptor) > 0) {
	cligand.add_clash();
      }
    }
  }
  // do scoring
  int nc = 0;
  int nc1 = 0;
  int nc10plus = 0;
  int nc5plus = 0;
  int nc5minus = 0;
  for (Coord &cligand : ligand) {
    if (cligand.get_clash() > 0) nc++;
    if (cligand.get_clash() == 1) nc1++;
    if (cligand.get_clash() > 10) nc10plus++;
    if (cligand.get_clash() > 5) nc5plus++;
    if (cligand.get_clash() <= 5 && cligand.get_clash() > 0) nc5minus++;
//    stringstream ss;
//    ss << setw(5) << cligand.get_clash() << ".00";
//    cout << cligand.get_line().replace(61, 5, ss.str()) << endl;
//
  }
  cout << "File=" << argv[1] << " SizeL=" << ligand.size() << " Nc=" << nc << " Nc1=" << nc1 << " Nc10plus=" << nc10plus 

       << " Nc5plus=" << (float) nc5plus 
       << " Nc5minus=" << (float) nc5minus 
       << " Nc1/SizeL=" << (float) nc/ligand.size() 
       << " Nc10plus/Nc1=" << (float) nc10plus/nc1 
       << " Nc5plus/Nc=" << (float) nc5plus/nc
       << " Nc5minus/Nc=" << (float) nc5minus/nc
       << " Nc5plus/Nc1=" << (float) nc5plus/nc1 << endl;
  myfile.close();
  return 0;
}
