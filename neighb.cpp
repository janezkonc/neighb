#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iomanip>
#include <assert.h>
#include <math.h>
using namespace std;


//ATOM   1705  OG1 THR B  17      -6.091  -9.736  -5.038  1.00  0.00           O

class Coord {
public:
  enum MolType {ligand, receptor};
private:
  MolType type;
  double c[3];
  char chain_id;
  string resn;
  int resi;
  string line;
public:
  Coord() {}
  Coord(const string &l, MolType t) : line(l), type(t) { set(line); }
  void set(const string &line) {
    c[0] = atof(line.substr(30, 8).c_str());
    c[1] = atof(line.substr(38, 8).c_str());
    c[2] = atof(line.substr(46, 8).c_str());
    chain_id = line[21];
    resn = line.substr(17, 3);
    resi = atoi(line.substr(22, 4).c_str());
  }
  double get(const int &i) const { return c[i]; }
  string get_line() const { return line; }
  MolType get_type() const { return type; }
  double distance(const Coord &crd) const { return sqrt(pow(c[0]-crd.get(0), 2) + pow(c[1]-crd.get(1), 2) + pow(c[2]-crd.get(2), 2)); }
  friend ostream& operator<<(ostream &os, const Coord &c) {
    return os << c.resn << ":" << c.chain_id << ":" << c.resi;
  }
};

class Grid {
  typedef vector<Coord > V0;
  typedef map<int, V0 > A0;
  typedef map<int, A0 > A1;
  typedef map<int, A1 > A2;
  A2 storage;
public:
  Grid() {}
  void insert(const Coord &c) {
    storage[floor(c.get(0))][floor(c.get(1))][floor(c.get(2))].push_back(c);
  }
  vector<Coord> get(const Coord &c, const double d) { // get all points <d A away from c and not type(c)
    int i = floor(c.get(0));
    int j = floor(c.get(1));
    int k = floor(c.get(2));
    int i_min = floor(c.get(0) - d);
    int j_min = floor(c.get(1) - d);
    int k_min = floor(c.get(2) - d);
    int i_max = floor(c.get(0) + d);
    int j_max = floor(c.get(1) + d);
    int k_max = floor(c.get(2) + d);
    V0 vc;
    for (i = i_min; i <= i_max; i++)
      for (j = j_min; j <= j_max; j++)
	for (k = k_min; k <= k_max; k++)
	  for (auto c2 : storage[i][j][k]) {
	    if (c.get_type() != c2.get_type())
	      vc.push_back(c2);
	  }
    return vc;
  }
  const void print() {
    for (auto a2 : storage) {
      for (auto a1 : storage[a2.first]) {
	for (auto a0 : storage[a2.first][a1.first]) {
	  for (auto c : storage[a2.first][a1.first][a0.first]) {
	    cout << "v[" << a2.first << "," << a1.first << "," << a0.first << "]=" << c << endl;
	  }
	}
      }
    }
  }
};


int main(int argc, char* argv[]) {
  assert(argc == 4);


  ifstream ligand_file, receptor_file;
  ligand_file.open (argv[1]);
  if (!ligand_file.is_open()) exit(1);

  receptor_file.open (argv[2]);
  if (!receptor_file.is_open()) exit(1);

  double d = atof(argv[3]);

  string line;
  vector<Coord> receptor, ligand;
  Grid grid;

  // read pdbs
  while (getline (ligand_file,line)) {
	  if (line.compare(0, 6, "ATOM  ") == 0 || line.compare(0, 6, "HETATM") == 0) {
		  ligand.push_back(Coord(line, Coord::ligand));
		  grid.insert(ligand.back());
	  }
  }

  while (getline (receptor_file,line)) {
	  if (line.compare(0, 6, "ATOM  ") == 0 || line.compare(0, 6, "HETATM") == 0) {
		  receptor.push_back(Coord(line, Coord::receptor));
		  grid.insert(receptor.back());
	  }
  }

  // calculate distances between receptor and ligand
  for (Coord &cligand : ligand) {
    vector <Coord> nearby = grid.get(cligand, d); 
    for (Coord &creceptor : nearby) {
      if (cligand.distance(creceptor) < d) {
	cout << creceptor << endl;
      }
    }
  }

  ligand_file.close();
  receptor_file.close();

  return 0;
}
